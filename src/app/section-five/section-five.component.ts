import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-section-five',
  templateUrl: './section-five.component.html',
  styleUrls: ['./section-five.component.css']
})
export class SectionFiveComponent {
  lists: string[] = [
    'Introduction',
    'Component API',
    'Input Properties',
    'Aliasing Input Properties',
    'Output Properties',
    'Passing Event Data',
    'Aliasing Output Properties',
    'Templates',
    'Styles',
    'View Encapsulation',
    'ngContent',
    'ngContainer',
    'Assignment 4: Like Component',
  ];

  five = {isFavorite: true};
  onFavoriteChange(isFavorite) {
    console.log("Favorite Change", isFavorite);
  }

}
