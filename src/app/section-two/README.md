# Section 2: TypeScript Fundamentals

## 2.1 What is TypeScript?
TypeScript is a superset of JavaScript. But it has additional features:
- Strong typing (Optional)
- Object-oriented features
- Compile-time errors
- Great tooling

Any valid JavaScript is valid TypeScript code. TypeScript need to be transpile to JavaScript.

## 2.2 Your First TypeScript Program

1. Install TypeScript in your computer `npm install -g typescript`
2. Verify TypeScript is installed by checking its version `tsc --version`
3. Transpile TypeScript file by issuing a command `tsc filename.ts`

Any valid JavaScript code is a valid TypeScript code.

## 2.3 Declaring Variables

In this section, Mosh explained about `let` versus `var` keyword.

## 2.4 Types

We can annotate type to variable in TypeScript.
```
let a: number
```
There are many types in TypeScript that we can use to annotate
```
let b: boolean;
let c: string;
let d: any;
let e: number[] = [1,2,3];
```

`enum` is great:
```
const ColorRed = 0;
const ColorGreen = 1;
const ColorBlue = 2;
```

Can be written like this:

```
enum Color = { Red = 0, Green = 1, Blue = 2};
let BackgroundColor = Color.Red;
```

## 2.5 Type Assertions

There are two ways to assert type to variable.
```
let message;
message = 'abc';
let endsWithC = (<string>message).endsWith('C'); // First way
let alternativeWay = (message as string).endsWith('C'); // Second way
```

Type assertions can help us using intellisense in VS Code.

## 2.6 Arrow Functions

```
let log = function(message) {
  console.log(message);
}

// can be written like this
let log = (message) => console.log(message);
```


## 2.7 Interfaces

Interface is a contract.
```
interface Point {
  x: number;
  y: number;
}

let drawPoint(point: Point) {
  // ...
}

drawPoint({
  x: 1,
  y: 3
});
```

## 2.8 Class

Group variables (properties) and functions (methods) that are highly related.
```
class Point {
  x: number;
  y: number;

  draw() {
    // ...
  }

  getDistance() {
    //...
  }
}
```


## 2.9 Object

An `object` is an instance of a `class`

```
class Point {
  x: number;
  y: number;

  draw() {
    console.log('X: ' + this.x + ', Y: ' + this.y);
  }

  getDistance() {
    //...
  }
}

let point = new Point(); // Creating an object
point.x = 1;
point.y = 2;
point.draw();
```

## 2.10 Constructors

A method that is called when we created an instance of a `class`.

```
class Point {
  x: number;
  y: number;

  constructor(x?: number, y?: number) {
    this.x = x;
    this.y = y;
  }

  draw() {
    console.log('X: ' + this.x + ', Y: ' + this.y);
  }
}

let point = new Point();
point.draw();
```

We can make constructor parameter to be optional by supplying `?` to the variable.

## 2.11 Access Modifiers

1. public
2. private
3. protected

This access modifiers can prevent other from accessing the properties of an object.

```
class Point {
  private x: number; // x is not accessible from outside
  private y: number; // cannot assign y like this anymore point.y = 1;

  constructor(x?: number, y?: number) {
    this.x = x;
    this.y = y;
  }

  draw() {
    console.log('X: ' + this.x + ', Y: ' + this.y);
  }
}
```

## 2.12 Access Modifiers in Constructor Parameter

We can shorten our previous code with this feature.

```
class Point {

  constructor(private x?: number, public y?: number) {}

  draw() {
    console.log('X: ' + this.x + ', Y: ' + this.y);
  }
}

let point = new Point(1, 2);
point.draw();
```

## 2.12 Properties

We use property to set and get the properties inside `class`, it also gives cleaner syntax.

```
class Point {

  constructor(private x?: number, public y?: number) {}

  draw() {
    console.log('X: ' + this.x + ', Y: ' + this.y);
  }

  get X() {
    return this.x;
  }

  set X(value) {
    if (value < 0)
      throw new Error('The value cannot be less than zero');

    this.x = value;
  }
}

let point = new Point(1, 2);
let x = point.X;
point.X = 10;
point.draw();
```

## 2.13 Modules

  Modules is used to separate code from each other so one file is not big.

  ```
  // point.ts

  export class Point {
  constructor(private x?: number, public y?: number) {}
  draw() {
    console.log('X: ' + this.x + ', Y: ' + this.y);
  }
  get X() {
    return this.x;
  }
  set X(value) {
    if (value < 0)
      throw new Error('The value cannot be less than zero');
    this.x = value;
  }
}


// main.ts

import { Point } from './point'

let point = new Point(1, 2);
let x = point.X;
point.X = 10;
point.draw();
  ```


