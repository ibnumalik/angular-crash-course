import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-section-two',
  templateUrl: './section-two.component.html',
  styleUrls: ['./section-two.component.css']
})
export class SectionTwoComponent {
  lists = [
    'what is typescript',
    'your first typescript program',
    'declaring variables',
    'types',
    'type assertion',
    'arrow functions',
    'interfaces',
    'classes',
    'objects',
    'constructors',
    'access modifiers',
    'access modifiers in constructor parameter',
    'properties',
    'modules',
  ];
}
