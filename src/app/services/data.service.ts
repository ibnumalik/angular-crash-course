import { NotFoundError } from './../common/not-found-error';
import { BadRequestError } from '../common/bad-request-error';
import { AppError } from './../common/app-error';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Injectable()
export class DataService {

  constructor(private url: string, private http: Http) { }

  getAll() {
    return this.http.get(this.url)
      .map(response => response.json())
      .catch(this.handleError);
  }

  create(resources) {
    return this.http.post(this.url, JSON.stringify(resources))
      .map(response => response.json())
      .catch(this.handleError)
  }

  update(resources) {
    return this.http.patch(this.url + '/' + resources.id, JSON.stringify(resources))
  }

  delete(resources) {
    return this.http.delete(this.url + '/' + resources.id)
      .map(response => response.json())
      .catch(this.handleError);
  }

  private handleError(error) {
    if (error.status === 404) {
      return Observable.throw(new NotFoundError());
    }

    if (error.status === 400) {
      return Observable.throw(new BadRequestError(error.json()));
    }

    return Observable.throw(new AppError(error))
  }
}
