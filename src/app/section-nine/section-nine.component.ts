import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-section-nine',
  templateUrl: './section-nine.component.html',
  styleUrls: ['./section-nine.component.css']
})
export class SectionNineComponent  {
  lists = [
    'introduction',
    'jsonplaceholder',
    'getting data',
    'creating data',
    'updating data',
    'deleting data',
    'oninit interface',
    'separation of concerns',
    'extracting a service',
    'handling errors',
    'handling unexpected errors',
    'handling expected errors',
    'throwing application-specific errors',
    'importing observable operators and factory methods',
    'global error handling',
    'extracting a reusable error handling',
    'extracting a reusable data service',
    'the map operator',
    'optimistic vs pessimistic updates',
    'observables vs promises',
    'assignment 8: github followers page'
  ];
}
