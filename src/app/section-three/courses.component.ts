import { Component } from '@angular/core';
import { CoursesService } from './courses.service';

@Component({
  selector: 'courses',
  templateUrl: './courses.component.html'
})

export class CoursesComponent {

  courses;

  constructor(service: CoursesService) {
    this.courses = service.getCourses();
  }

    isActive = true;

}
