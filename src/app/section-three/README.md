# Section 3: Angular Fundamentals

## 3.1 Building Blocks of Angular Apss?

Angular apps are build on components and modules.

## 3.2 Components
### Naming convention

```
courses.component.ts
```

## 3.3 Generating Components Using Angular CLI
We can quickly create Angular component using command below
```
ng g c course
```

## 3.4 Templates
```
@Component({
  selector: 'courses',
  template: `
    <h2>This is template</h2>
    <p>{{ This curly braces is `interpolation` }}</p>
  `
});
```

## 3.5 Directives

Directive use to manipulate DOM element.

```
<li *ngFor="let course of courses">{{ course }}</li>
```

## 3.6 Services

Separate call to server logic in service.

## 3.7 Dependency Injection

Instead of creating an instance in each controller, we separate it using DI and let Angular instantiate it. So to say we decouple service from controller for easier testing.

Register service under providers in `app.module.ts`. Service is singleton.

## 3.8 Generating Services using Angular CLI
Quickly create service using Angular CLI.
```
ng g s email
```