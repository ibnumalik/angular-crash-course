import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-section-three',
  templateUrl: './section-three.component.html',
  styleUrls: ['./section-three.component.css']
})
export class SectionThreeComponent {

  lists = [
    'building blocks of angular apps',
    'components',
    'generating components using angular cli',
    'templates',
    'directives',
    'services',
    'dependencies injection',
    'generating services using angular cli'
  ];

}
