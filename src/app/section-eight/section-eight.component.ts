import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-section-eight',
  templateUrl: './section-eight.component.html',
  styleUrls: ['./section-eight.component.css']
})
export class SectionEightComponent {
  lists = [
    'introduction',
    'building a bootstrap form',
    'creating controls programmatically',
    'adding validation',
    'specific validation errors',
    'implementing custom validation',
    'asynchronous operations',
    'showing a loader image',
    'validating the form input upon submit',
    'nested formgroups',
    'formarray',
    'formbuilder',
    'assignment 7: change password form'
  ];
}
