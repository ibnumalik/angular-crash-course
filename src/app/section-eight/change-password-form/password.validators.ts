import { AbstractControl, ValidationErrors } from '@angular/forms';

export class PasswordValidators {
  static validOldPassword(control: AbstractControl) : Promise<ValidationErrors | null> {
    return new Promise((resolve, reject) => {
      if(control.value !== '1234') {
        resolve({ invalidOldPassword: true });
      } else {
        resolve(null);
      }
    });
  }

  static passwordShouldMatch(control: AbstractControl) {
    if(control.get('newPassword').value !== control.get('confirmPassword').value) {
      return { passwordDidNotMatch: true };
    } else {
      return null;
    }
  }
}
