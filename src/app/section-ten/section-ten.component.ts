import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-section-ten',
  templateUrl: './section-ten.component.html',
  styleUrls: ['./section-ten.component.css']
})
export class SectionTenComponent {
  lists = [
    'introduction',
    'routing in a nutshell',
    'configuring routes',
    'routerOutlet',
    'routerLink',
    'routerLinkActive',
    'getting the route parameters',
    'why route parameters are observables',
    'routes with multiple parameters',
    'query parameters',
    'subscribing to multiple observables',
    'the switchmap operator',
    'programmatic navigation',
    'assignment 9: blog archives',
  ];
}
