import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  routes: any[] = [
    {
      name: 'Section 1',
      link: '/section-1'
    },
    {
      name: 'Section 2',
      link: '/section-2'
    },
    {
      name: 'Section 3',
      link: '/section-3'
    },
    {
      name: 'Section 4',
      link: '/section-4'
    },
    {
      name: 'Section 5',
      link: '/section-5'
    },
    {
      name: 'Section 6',
      link: '/section-6'
    },
    {
      name: 'Section 7',
      link: '/section-7'
    },
    {
      name: 'Section 8',
      link: '/section-8'
    },
    {
      name: 'Section 9',
      link: '/section-9'
    },
    {
      name: 'Section 10',
      link: '/section-10'
    },
  ];
}
