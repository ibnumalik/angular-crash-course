import { CoursesService } from './section-three/courses.service';
import { SectionSixComponent } from './section-six/section-six.component';
import { AppErrorHandler } from './common/app-error-handler';
import { ErrorHandler } from '@angular/core';
import { SummaryPipe } from './section-four/summary.pipe';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { CoursesComponent } from './section-three/courses.component';
import { AuthorsComponent } from './section-three/authors/authors.component';
import { FavoriteComponent } from './favorite/favorite.component';
import { LikeComponent } from './section-five/like/like.component';
import { InputFormatDirective } from './section-six/input-format.directive';
import { ZippyComponent } from './section-six/zippy/zippy.component';
import { ContactFormComponent } from './section-seven/contact-form/contact-form.component';
import { CoursesFormComponent } from './section-seven/courses-form/courses-form.component';
import { SignupFormComponent } from './section-eight/signup-form/signup-form.component';
import { NewCourseFormComponent } from './section-eight/new-course-form/new-course-form.component';
import { ChangePasswordFormComponent } from './section-eight/change-password-form/change-password-form.component';
import { PostsComponent } from './section-nine/posts/posts.component';
import { PostService } from './section-nine/posts/post.service';
import { GithubFollowersComponent } from './section-nine/github-followers/github-followers.component';
import { GithubService } from './services/github.service';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { GithubProfileComponent } from './section-ten/github-profile/github-profile.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { SectionFourComponent } from './section-four/section-four.component';
import { TitleCaseComponent } from './section-four/title-case/title-case.component';
import { TitleCasePipe } from './section-four/title-case.pipe';
import { SectionOneComponent } from './section-one/section-one.component';
import { SectionTwoComponent } from './section-two/section-two.component';
import { SectionThreeComponent } from './section-three/section-three.component';
import { SectionFiveComponent } from './section-five/section-five.component';
import { SectionSevenComponent } from './section-seven/section-seven.component';
import { SectionEightComponent } from './section-eight/section-eight.component';
import { SectionNineComponent } from './section-nine/section-nine.component';
import { SectionTenComponent } from './section-ten/section-ten.component';
import { BootstrapPanelComponent } from './section-five/bootstrap-panel/bootstrap-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent,
    AuthorsComponent,
    SummaryPipe,
    FavoriteComponent,
    LikeComponent,
    SectionSixComponent,
    InputFormatDirective,
    ZippyComponent,
    ContactFormComponent,
    CoursesFormComponent,
    SignupFormComponent,
    NewCourseFormComponent,
    ChangePasswordFormComponent,
    PostsComponent,
    GithubFollowersComponent,
    NavbarComponent,
    HomeComponent,
    GithubProfileComponent,
    NotFoundComponent,
    SectionFourComponent,
    TitleCaseComponent,
    TitleCasePipe,
    SectionOneComponent,
    SectionTwoComponent,
    SectionThreeComponent,
    SectionFiveComponent,
    SectionSevenComponent,
    SectionEightComponent,
    SectionNineComponent,
    SectionTenComponent,
    BootstrapPanelComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'section-1',
        component: SectionOneComponent
      },
      {
        path: 'section-2',
        component: SectionTwoComponent
      },
      {
        path: 'section-3',
        component: SectionThreeComponent
      },
      {
        path: 'section-4',
        component: SectionFourComponent
      },
      {
        path: 'section-5',
        component: SectionFiveComponent
      },
      {
        path: 'section-6',
        component: SectionSixComponent
      },
      {
        path: 'section-7',
        component: SectionSevenComponent
      },
      {
        path: 'section-8',
        component: SectionEightComponent
      },
      {
        path: 'section-9',
        component: SectionNineComponent
      },
      {
        path: 'section-10',
        component: SectionTenComponent
      },
      {
        path: 'followers/:id/:username',
        component: GithubProfileComponent
      },
      {
        path: '**',
        component: NotFoundComponent
      }
    ])
  ],
  providers: [
    CoursesService,
    PostService,
    { provide: ErrorHandler, useClass: AppErrorHandler },
    GithubService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
