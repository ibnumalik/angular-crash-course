import { Component, OnInit } from '@angular/core';
import { FavoriteChangedArgs } from '../favorite/favorite.component';

@Component({
  selector: 'section-one',
  templateUrl: './section-one.component.html',
  styleUrls: ['./section-one.component.css']
})
export class SectionOneComponent {
  lists = [
    'what is angular',
    'architecture of angular apps',
    'setting up the development environment',
    'your first angular app',
    'structure of angular projects',
    'webpack',
    'angular version history',
    'how to take this course',
  ];
  post = {
    title: "Title",
    isFavorite: true
  }
  title = 'app';
  onFavoriteChanged(favSelected: FavoriteChangedArgs) {
    console.log('Favorite changed', favSelected);
  }
  tweet = {
    body: '...',
    likesCount: 20,
    isLike: false
  }
}
