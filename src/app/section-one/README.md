# Section 1: Introduction

## 1.1 What is Angular?
A framework for building client applications in HTML, CSS and JavaScript/TypeScript.

### Why do we need Angular?
- Gives applications a clean structure.
- Includes a lot of reusable code
- Make our applications more testable

Angular makes life easier!

## 1.2 Architecture of Angular Apps

1. Front-end
    1. HTML Templates
    2. Presentation Logic
2. Back-end
    1. Data + APIs
    2. Business Logic

## 1.3 Setting Up the Development Environment

1. Install [nodejs](https://nodejs.org/en/download/).
    1. Open terminal, type `node --version`
    2. Install angular cli `npm install -g @angular/cli`
    3. Check angular cli installed `ng --version`

## 1.4 Your First Angular App

1. Create new angular app by typing `ng new {project name}` in terminal.
2. Use [Visual Studio Code](https://code.visualstudio.com/) for code editor.
3. Start up your development server `ng serve`.
4. Open the browser with `http://localhost:4200` address.

## 1.5 Structure of Angular Projects

```
app
│   e2e/
│   node_modules/
│
├─── src/
│   │
│   ├── app/
│   │   │
│   │   ├── app.module.ts
│   │   └── app.component.ts
│   │
│   ├── assets/
│   │   │
│   │   └── images/
│   │
│   ├── environments/
│   ├── index.html
│   ├── main.ts
│   ├── polyfill.ts
│   ├── styles.css
│   └── tests.ts
│
├─── .editorconfig
├─── karma.conf.js
├─── package.json // important file
├─── protractor.conf.js
├─── tsconfig.json
└─── tslint.json
```

## 1.6 Webpack

Angular CLI uses webpack which is automation tool to bundle files. We have `main.bundle.js`, `styles.bundle.js` and `vendor.bundle.js` build by webpack.

Webpack will compile our source and reload browser for us, this feature is called `hot module replacment`.

The bundle is injected automatically to `index.html`.

## 1.7 Angular Version History

2010 - AngularJS (Overly complex)

2016 - Angular (Complete rewrite and it is different than AngularJS and make a lot of developer angry)

Angular jump from Angular 2 to Angular 4, it confuses a lot of developers. The main cause is because `@angular/router` library version is version 3 while other library still in version 2, to avoid confusion Angular team bump all library to version 4.