import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-section-seven',
  templateUrl: './section-seven.component.html',
  styleUrls: ['./section-seven.component.css']
})
export class SectionSevenComponent  {
  lists = [
    'introduction',
    'building a bootstrap form',
    'types of forms',
    'ngModel',
    'adding validation',
    'specific validation errors',
    'styling invalid input fields',
    'cleaner templates',
    'ngForm',
    'ngModelGroup',
    'control classes and directives',
    'disabling the submit button',
    'working with check boxes',
    'working with drop-down lists',
    'working with radio buttons',
    'assignment 6: course form'
  ];
}
