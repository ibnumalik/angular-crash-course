import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'section-four',
  templateUrl: './section-four.component.html',
  styleUrls: ['./section-four.component.css']
})
export class SectionFourComponent  {
  lists = [
    'Introduction',
    'Property Binding',
    'Attribute Binding',
    'Adding Bootstrap',
    'Class Binding',
    'Class Binding',
    'Style Binding',
    'Event Binding',
    'Event Filtering',
    'Template Variables',
    'Two-Way Binding',
    'Pipes',
    'Custom Pipes',
    'Assignment 2: Favorite Component',
    'Assignment 3: Title Casing',
  ];

  imageUrl = "https://picsum.photos/458/354";
  colSpan = "Attribute binding rocks!";
  isLarge = true;
  isDanger = true;
  email2 = "secondway@2waybinding.com";
  email3 = "firstway@2waybinding.com"
  longText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis et diam diam. Morbi malesuada risus pulvinar nisi congue, ac pretium sem finibus. Praesent efficitur ex id felis tincidunt, et imperdiet metus dignissim. Curabitur elementum feugiat sodales. Maecenas sit amet ante vel metus accumsan euismod vel at magna. Donec et ex iaculis sem euismod tempor sed et enim. Suspendisse condimentum quam id nibh condimentum hendrerit vel in ante.";

  course = {
    title: "The Complete Angular Course",
    rating: 4.9745,
    students: 30123,
    price: 190.95,
    releaseDate: new Date(2016, 3, 1)
  };
  onDivClick() {
    console.log("Div was clicked");
  }
  onSave($event) {
    $event.stopPropagation();
    console.log("Button was clicked", $event);
  }
  onKeyUp() {
    console.log('Enter was pressed');
  }
  onEmailEnter(email) {
    console.log(email)
  }
  onChangeEmail() {
    console.log(this.email2);
  }
  onEmailKeyUp() {
    console.log(this.email3);
  }
}
