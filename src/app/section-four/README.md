# Section 4: Displaying Data and Handling Events

## 4.1 Property Binding

It works only on one way, components to the DOM.

```html
<img [src]="title" />

<h2 [textContent]="title"></h2>
```

## 4.2 Attribute Binding

Notice that below we bind with `attr` object because colspan is not a DOM element but it is HTML element.

```html
<table>
  <tr>
    <td [attr.colspan]="colSpan" ></td>
  </tr>
</table>
```

## 4.3 Class Binding

```html
<button class="btn btn-primary" [class.btn-lg]="isLarge">Save</button>
```

## 4.4 Style Binding

```html
<button class="btn" [style.backgroundColor]="isDanger ? 'peach' : 'cornflowerblue'">
  Save
</button>
```

## 4.5 Event Binding

We can bind event to element to call method. Example below we bind `click` to a button.
```html
<div (click)="onDivClick()">
    <button class="btn btn-default" (click)="onSave($event)">
      Save Event
    </button>
</div>
```

In class we define `onSave()` method.
```javascript
onSave() {
  console.log("Button is clicked");
}
```

## 4.6 Event Filtering
```html
<input class="form-control" (keyup.enter)="onKeyUp()" />
```

## 4.5 Template Variable
```html
<input #email (keyup.enter)="onEmailEnter(email.value)" />
```