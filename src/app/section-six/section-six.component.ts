import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'section-six',
  templateUrl: './section-six.component.html',
  styleUrls: ['./section-six.component.css']
})
export class SectionSixComponent {
  lists = [
    'introduction',
    'ngIf',
    'hidden property',
    'ngSwitchCase',
    'ngFor',
    'ngFor and Change Detection',
    'ngFor and Trackby',
    'The Leading Asterisk',
    'ngClass',
    'ngStyle',
    'safe traversal operator',
    'creating custom directives',
    'assignment 5: ZippyComponent',
  ];
  courses = [
    {id: 1, name: 'Learn Angular 4'},
    {id: 2, name: 'Git Good'},
    {id: 3, name: 'Laravel 5 and Beyond'},
  ];
  courseslists;
  viewMode = 'map';
  isSelected;
  canSave = true;
  task = {
    title: 'Learn Angular',
    assignee: null
  };
  inputFormat = 'lowercase';

  onAdd() {
    this.courses.push({id: 4, name: 'Gulp Task Automation'});
  }

  onRemove(course) {
    let index = this.courses.indexOf(course);
    this.courses.splice(index, 1);
  }

  onEdit(course) {
    course.name = 'Modern PHP Development';
  }

  loadCourses() {
    this.courseslists = [
      {id: 1, name: 'Python Machine Learning'},
      {id: 2, name: 'Mastering Docker'},
      {id: 3, name: 'Statistics for Machine Learning'},
    ];
  }

  trackCourse(index, course) {
    return course ? course.id : undefined;
  }

  onClick() {
    this.isSelected = !this.isSelected;
  }

}
