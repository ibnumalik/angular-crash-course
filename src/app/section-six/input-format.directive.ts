import { Directive, HostListener, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appInputFormat]'
})
export class InputFormatDirective {
  @Input('appInputFormat') value;
  constructor(private el: ElementRef) { }

  @HostListener('blur') onBlur() {

    let value : string = this.el.nativeElement.value;
    if (this.value === 'lowercase') {
      console.log(this.value);
      this.el.nativeElement.value = value.toLowerCase();
    } else {
      console.log(this.value);
      this.el.nativeElement.value = value.toUpperCase();
    }

  }


}
