# Angular Crash Course

This project is aimed to make myself familiar with Angular syntax and understand how it operate differently than AngularJS. This repository is code practice that I code along with Mosh in his course called Angular Crash Course for Busy Developer.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.